<?php

namespace Drupal\delegate_permissions;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\Role;
use Drupal\user\PermissionHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * DelegatePermissionsHelper service.
 *
 * @see hook_bypassed_provider_map()
 * @see plugin_api
 */
class DelegatePermissionsHelper {

  /**
   * The user.permissions service.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a DelegatePermissionsHelper object.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The user.permissions service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, AccountInterface $account, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->permissionHandler = $permission_handler;
    $this->currentUser = $account;
    $this->roleStorage = $entity_type_manager->getStorage('user_role');
    $this->configFactory = $config_factory;
  }

  /**
   * Gets a role weights.
   *
   * @param \Drupal\user\Entity\Role|string $role
   *   The role.
   *
   * @return int
   *   The role weight.
   */
  protected function getRoleWeight($role) {
    // If we get the role id, we need to load it.
    if (is_string($role)) {
      $role = Role::load($role);
    }

    return $role->getWeight();
  }

  /**
   * Gets all available roles excluding locking roles.
   *
   * @return \Drupal\user\RoleInterface[]
   *   An array of role objects.
   */
  protected function getRoles() {
    $roles = $this->roleStorage->loadMultiple();
    $roles = array_diff_key($roles, array_flip(['authenticated', 'anonymous']));

    return $roles;
  }

  /**
   * Gets the lower roles for the current user role.
   *
   * @return \Drupal\user\RoleInterface[]
   *   An array of role objects.
   */
  public function getLowerRoles() {
    $current_weights = [];
    // Get all roles from current user.
    foreach ($this->currentUser->getRoles(TRUE) as $current_role) {
      $current_weights[$current_role] = $this->getRoleWeight($current_role);
    }
    // If current user has administer permission (ie uid 1).
    if ($this->currentUser->hasPermission('administer permissions')) {
      $base_weight = '-9999';
    }
    else {
      // Set the lower role from Current user to use as base.
      $base_weight = $current_weights ? (int) max($current_weights) : '9999';
    }

    $allowed_roles = [];
    // Get all present roles.
    foreach ($this->getRoles() as $role_name => $role) {
      // Set the allowed roles to the ones lower than the base.
      if ($this->getRoleWeight($role) > $base_weight) {
        $allowed_roles[$role_name] = $role;
      }
    }

    return $allowed_roles;
  }

  /**
   * Gets the filtered roles restricted to the ones for the current user.
   *
   * @return array
   *   An array of permissions.
   */
  public function getRestrictedPerms() {
    $permissions = $this->getAllPermissions();
    $config = $this->configFactory->get('delegate_permissions.settings');
    $not_delegable = $config->get('not_delegable');

    $bypassed_providers = $this->calculateBypassedProviders();
    foreach ($permissions as $perm_name => $perm) {
      // Remove if permission is not delegable.
      if (in_array($perm_name, $not_delegable)) {
        unset($permissions[$perm_name]);
        continue;
      }
      // Skip if have a provider bypass permission.
      if (in_array($perm['provider'], $bypassed_providers)) {
        continue;
      }
      if ($this->currentUser->hasPermission($perm_name) == FALSE) {
        unset($permissions[$perm_name]);
      }
    }
    return $permissions;
  }

  /**
   * Gets the array of bypass permissions.
   *
   * @return array
   *   An associative array keyed by provider.
   */
  public function getBypassedProvidersMap() {
    $bypassed_providers_map = [
      'node' => 'bypass node access',
      'taxonomy' => 'administer taxonomy',
    ];
    // Allow modules to alter the array map.
    \Drupal::moduleHandler()->alter('bypassed_provider_map', $bypassed_providers_map);

    return $bypassed_providers_map;
  }

  /**
   * Gets the list of providers the current user has the permission to bypass.
   *
   * @param \Drupal\user\Entity\Role $role
   *   The role to check the permissions.
   *
   * @return array
   *   An array of bypassed providers.
   */
  public function calculateBypassedProviders(Role $role = NULL) {
    $bypass_providers_map = $this->getBypassedProvidersMap();
    $bypassed_providers = [];
    foreach ($bypass_providers_map as $provider => $permission) {
      if (($role && $role->hasPermission($permission)) ||
        $this->currentUser->hasPermission($permission)) {
        $bypassed_providers[] = $provider;
      }
    }
    return $bypassed_providers;
  }

  /**
   * Gets all available permissions.
   */
  public function getAllPermissions() {
    return $this->permissionHandler->getPermissions();
  }

}
