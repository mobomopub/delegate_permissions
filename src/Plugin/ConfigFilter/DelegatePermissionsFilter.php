<?php

namespace Drupal\delegate_permissions\Plugin\ConfigFilter;

use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\delegate_permissions\DelegatePermissionsHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a RoleSplitFilter.
 *
 * @ConfigFilter(
 *   id = "delegate_permissions",
 *   label = @Translation("Delegate Permissions"),
 *   weight = 0,
 *   status = TRUE,
 *   storages = {"config.storage.sync"}
 * )
 */
class DelegatePermissionsFilter extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Permissions Helper service.
   *
   * @var \Drupal\delegate_permissions\DelegatePermissionsHelper
   */
  protected $permissionsHelper;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Overrides Drupal\Component\Plugin\PluginBase::__construct().
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\delegate_permissions\DelegatePermissionsHelper $permissions_helper
   *   Permissions Helper service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, DelegatePermissionsHelper $permissions_helper, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->permissionsHelper = $permissions_helper;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('delegate_permissions.helper'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function filterRead($name, $data) {
    if (!$this->hasDelegablePerms($name)) {
      return parent::filterRead($name, $data);
    }
    $data['permissions'] = $this->calculatePerms($data['permissions'], $name);
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function filterReadMultiple(array $names, array $data) {
    foreach ($names as $name) {
      if ($this->hasDelegablePerms($name)) {
        // Filter managed roles individually.
        $data[$name] = $this->filterRead($name, $data[$name]);
      }
    }

    return $data;
  }

  /**
   * Return whether the configuration is a role with delegable permissions.
   *
   * @param string $config_name
   *   The name of the config to check.
   *
   * @return bool
   *   Whether the config is a role that has delegable permissions.
   */
  protected function hasDelegablePerms($config_name) {
    if (strpos($config_name, 'user.role.') === 0) {
      $higher_role = $this->getHigherRole($config_name);
      if ($higher_role) {
        if ($higher_role->hasPermission('allow delegate permissions')) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Calculate the new permissions.
   *
   * @param array $perms_from_config
   *   The permissions in the role config.
   * @param string $config_name
   *   The name of the config to check.
   *
   * @return array
   *   The calculated permissions to set.
   */
  protected function calculatePerms(array $perms_from_config, $config_name) {
    // @todo: Check if we get a diff between DB and files, if diff then get config file. See drush cst.
    $not_delegable = $this->configFactory->get('delegate_permissions.settings')->get('not_delegable');
    $parent_permissions = $this->getHigherRolePermissions($config_name);
    $role_current_permissions = $this->getRolePermissions($config_name);
    // Merging config and current permissions.
    $merged_permissions = array_unique(array_merge($perms_from_config, $role_current_permissions));
    // Permissions to be removed.
    $permissions_to_remove = array_diff($role_current_permissions, $perms_from_config);
    // Permissions that can be removed.
    $not_removable_permissions = array_diff($parent_permissions, $not_delegable);
    // Checks if perms to remove are not 'delegable'.
    $permissions_to_remove = array_diff($permissions_to_remove, $not_removable_permissions);
    // Calculated permissions.
    $permissions = array_diff($merged_permissions, $permissions_to_remove);
    return $permissions;
  }

  /**
   * Get the permissions of a role from config name.
   *
   * @param string $config_name
   *   The name of the config to check.
   *
   * @return array
   *   The permissions of the role.
   */
  protected function getRolePermissions($config_name) {
    $role_id = $this->getRoleIdFromConfigName($config_name);
    /** @var \Drupal\user\Entity\Role $role */
    $role = $this->entityTypeManager->getStorage('user_role')->load($role_id);
    return $role->getPermissions();
  }

  /**
   * Get the permissions for a higher role from config name.
   *
   * @param string $config_name
   *   The name of the config to check.
   *
   * @return array
   *   The permissions of the higher role.
   */
  protected function getHigherRolePermissions($config_name) {
    $permissions = [];
    // Get a list of permission keyed by provider.
    $all_permissions = $this->permissionsHelper->getAllPermissions();
    $permissions_by_provider = [];
    foreach ($all_permissions as $key => $permission) {
      $permissions_by_provider[$permission['provider']][] = $key;
    }
    if ($higher_role = $this->getHigherRole($config_name)) {
      $permissions = $higher_role->getPermissions();
      // Adding bypassed permissions.
      $bypassed_providers = $this->permissionsHelper->calculateBypassedProviders($higher_role);
      foreach ($bypassed_providers as $provider) {
        $permissions = array_unique(array_merge($bypassed_providers, $permissions_by_provider[$provider]));
      }
    }
    return $permissions;
  }

  /**
   * Get the role id from config name.
   *
   * @param string $config_name
   *   The name of the config to check.
   *
   * @return string
   *   The role id.
   */
  protected function getRoleIdFromConfigName($config_name) {
    // @todo: Maybe we can check for the role ìd in the yaml?
    preg_match('/^user.role.(.*)/', $config_name, $matches);
    return $matches[1];
  }

  /**
   * Get the higher role.
   *
   * @param string $config_name
   *   The name of the config to check.
   *
   * @return mixed
   *   The role or FALSE if does not have higher role.
   */
  protected function getHigherRole($config_name) {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $roles = array_diff_key($roles, array_flip(['authenticated', 'anonymous']));
    $roles_hierarchy = array_keys($roles);
    $role_id = $this->getRoleIdFromConfigName($config_name);
    $index = array_search($role_id, $roles_hierarchy);
    if ($index) {
      $higher_role = $roles_hierarchy[$index - 1];
      /** @var \Drupal\user\Entity\Role $higher_role */
      $higher_role = $this->entityTypeManager->getStorage('user_role')->load($higher_role);
      return $higher_role;
    }
    return FALSE;
  }

}
