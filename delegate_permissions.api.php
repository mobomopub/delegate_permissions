<?php

/**
 * @file
 * Hooks for delegate_permissions module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the bypassed permissions definitions.
 *
 * @param array &$bypassed_providers_map
 *   An associative array mapping permissions provider with grant/bypass permission name.
 *
 * @see \Drupal\delegate_permissions\DelegatePermissionsHelper::getBypassedProvidersMap()
 */
function hook_bypassed_provider_map_alter(array &$bypassed_providers_map) {

}

/**
 * @} End of "addtogroup hooks".
 */
